# Syväoppimisen projekti - kevät 2023

Harjoitustehtävä tekoälykurssia varten. Vapaavalintainen aihe

Tehtävänanto löytyy [täältä](osa4_tehtava.md)

Raportti löytyy [Notebookista](HousePricesRegression.ipynb)